import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    cartItems: JSON.parse(localStorage.getItem("cartItems")) || [],
    showCartModal: false,
}

const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        actionAddToCart: (state, {payload}) => {
            const indexItem = state.cartItems.findIndex((item) => item.id === payload.id);
    
            if (indexItem === -1) {
                const newItem = {
                    id: payload.id,
                    name: payload.name,
                    price: payload.price,
                    image: payload.image,
                    quantity: 1,
                };
                const mergedCartItems = [...state.cartItems, newItem];
                state.cartItems = mergedCartItems;
                localStorage.setItem("cartItems", JSON.stringify(mergedCartItems));
            } else {
                const updatedItem = {
                    ...state.cartItems[indexItem],
                    quantity: state.cartItems[indexItem].quantity + 1,
                };
                const updatedCartItems = [...state.cartItems];
                updatedCartItems.splice(indexItem, 1, updatedItem);
    
                state.cartItems = updatedCartItems;
                localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
            }
        },

        actionRemoveFromCart: (state, {payload}) => {
            const updatedCartItems = [...state.cartItems];
    
            const indexItem = updatedCartItems.findIndex(
                (item) => item.id === payload.id
            );
    
            if (indexItem !== -1) {
                updatedCartItems[indexItem].quantity -= 1;
    
                if (updatedCartItems[indexItem].quantity === 0) {
                    updatedCartItems.splice(indexItem, 1);
                }
    
                state.cartItems = updatedCartItems;
                localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
            }
        },

        actionsRemoveItemFromCart: (state, {payload}) => {
            const updatedCartItems = state.cartItems.filter(
                (item) => item.id !== payload.id
            );
            state.cartItems = updatedCartItems;
            localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
            console.log(updatedCartItems);
        },

        actionsShowCartModal: (state, ) => {
            state.showCartModal = !state.showCartModal
        },
    }
});

export const {
    actionAddToCart, 
    actionRemoveFromCart, 
    actionsRemoveItemFromCart, 
    actionsShowCartModal
} = cartSlice.actions;

export default cartSlice.reducer;