import { createSlice } from "@reduxjs/toolkit";
import { sendRequest } from "../helpers/sendRequest.js";

const initialState = {
    products: [],
    showConfirmationModal: false,
    isLoading: true,
}

const appSlice = createSlice({
    name: "app",
    initialState,
    reducers: {
        actionAddToProducts: (state, {payload}) => {
            state.products = [...payload];
        },

        actionsShowConfirmationModal: (state, ) => {
            state.showConfirmationModal = !state.showConfirmationModal
        },

        actionLoading: (state, {payload}) =>{
            state.isLoading = payload
        },

    }
});

export const { actionAddToProducts, actionsShowConfirmationModal, actionLoading } = appSlice.actions;

export const actionFetchProducts = () => (dispatch) => {
    dispatch(actionLoading(true));
    return sendRequest("products.json")
        .then((products) => {
            dispatch(actionAddToProducts(products));
            dispatch(actionLoading(false));
        })
}

export default appSlice.reducer;