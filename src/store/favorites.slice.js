import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    favoriteItems: JSON.parse(localStorage.getItem("favoriteItems")) || [],
    showFavoritesModal: false,
}

const favoritesSlice = createSlice({
    name: "favorites",
    initialState,
    reducers: {
        actionAddFavorites: (state, {payload}) => {
            const indexItem = state.favoriteItems.findIndex(
                (item) => item.id === payload.id
            );
    
            if (indexItem === -1) {
                const newItem = {
                    id: payload.id,
                    name: payload.name,
                    price: payload.price,
                    image: payload.image,
                };
                const mergedFavoriteItems = [...state.favoriteItems, newItem];
    
                state.favoriteItems = mergedFavoriteItems;
                localStorage.setItem(
                    "favoriteItems",
                    JSON.stringify(mergedFavoriteItems)
                );
            } else {
                const updatedFavoriteItems = state.favoriteItems.filter(
                    (item) => item.id !== payload.id
                );
                state.favoriteItems = updatedFavoriteItems;
                localStorage.setItem(
                    "favoriteItems",
                    JSON.stringify(updatedFavoriteItems)
                );
            }
        },

        actionRemoveFromFavorites:(state, {payload}) => {
            const updatedFavoriteItems = state.favoriteItems.filter(
                (item) => item.id !== payload.id
            );
    
            state.favoriteItems = updatedFavoriteItems;
            localStorage.setItem(
                "favoriteItems",
                JSON.stringify(updatedFavoriteItems)
            );
        },

        actionsShowFavoritesModal: (state, ) => {
            state.showFavoritesModal = !state.showFavoritesModal
        },

    }
});

export const {actionAddFavorites, actionRemoveFromFavorites, actionsShowFavoritesModal } = favoritesSlice.actions;

export default favoritesSlice.reducer;