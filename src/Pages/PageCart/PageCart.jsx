import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import {
    selectFavoritesItems,
    selectCartItems,
} from "../../selectors/index.js";
import { actionAddFavorites } from "../../store/favorites.slice.js";
import { actionsShowCartModal } from "../../store/cart.slice.js";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from "../../components/Button";
import "./PageCart.scss";

export default function PageCart({ handleRemoveClick }) {
    const dispatch = useDispatch();
    const favoriteItems = useSelector(selectFavoritesItems);
    const cartItems = useSelector(selectCartItems);

    const inFavorites = (product) => {
        if (favoriteItems.some((item) => item.id === product.id)) {
            return true;
        }
        return false;
    };

    const productsInCart = cartItems?.map((item) => {
        return (
            <div key={item.id} className="product-card card_in-cart">
                <div className="image-wrapper">
                    <img src={item.image} alt={item.name} />
                </div>
                <span
                    className="close-card"
                    onClick={() => handleRemoveClick(item)}
                >
                    <div className="close-card_item">&times;</div>
                </span>
                <h3 className="product-name">{item.name}</h3>

                <div className="card-footer">
                    <Button
                        className="add-favorite"
                        onClick={() => dispatch(actionAddFavorites(item))}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="xs"
                            style={{
                                color: inFavorites(item)
                                    ? "#f4f88b"
                                    : "#9a9a9c",
                            }}
                        />
                    </Button>
                    <p className="product-price">{item.price} грн</p>
                </div>
                <Button
                    className="remove-to-cart"
                    onClick={() => dispatch(actionsShowCartModal())}
                >
                    Додати ще?
                </Button>
            </div>
        );
    });

    return (
        <div className="cart-page">
            <h2 className="cart-page__title">Кошик</h2>
            {productsInCart.length === 0 ? (
                <div className="cart-page__none">
                    Кошик порожній.
                    <br /> Додайте будь-ласка товар
                </div>
            ) : (
                <div className="cart-items">{productsInCart}</div>
            )}
        </div>
    );
}

PageCart.propTypes = {
    id: PropTypes.number,
    product: PropTypes.object,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    inFavorites: PropTypes.func,
    handleRemoveClick: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
};
