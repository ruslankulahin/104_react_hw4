import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { actionRemoveFromFavorites } from "../../store/favorites.slice.js";
import { selectFavoritesItems } from "../../selectors/index.js";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from "../../components/Button";
import "./PageFavorites.scss";

export default function PageFavorites() {
    const dispatch = useDispatch();
    const favoriteItems = useSelector(selectFavoritesItems);

    const handleItemFavoriteClose = (product) => {
        dispatch(actionRemoveFromFavorites(product));
    };

    const productsInFavorites = favoriteItems?.map((item) => {
        return (
            <div key={item.id} className="product-card card_in-favorites">
                <div className="image-wrapper">
                    <img src={item.image} alt={item.name} />
                </div>
                <h3 className="product-name">{item.name}</h3>

                <div className="card-footer">
                    <Button
                        className="add-favorite"
                        onClick={() => handleItemFavoriteClose(item)}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="xs"
                            style={{ color: "#f4f88b" }}
                        />
                    </Button>

                    <p className="product-price">{item.price} грн</p>
                </div>
            </div>
        );
    });

    return (
        <div className="favorites-page">
            <h2 className="favorites-page__title">Обрані</h2>
            {productsInFavorites.length === 0 ? (
                <div className="favorites-page__none">
                    Жодного товару не обрано.
                    <br /> Додайте будь-ласка свій улюблений товар
                </div>
            ) : (
                <div className="favorites-items">{productsInFavorites}</div>
            )}
        </div>
    );
}

PageFavorites.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    }),
    favoriteItems: PropTypes.array,
    handleItemFavoriteClose: PropTypes.func,
};
